
var mysql = require('mysql');
const con = mysql.createConnection({
    host: "localhost",
    user: "admin",
    password: "admin",
    database: "facebookClone"
  });
  
  con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
    con.query("CREATE DATABASE IF NOT EXISTS firstProject", function (err, result) {
        if (err) throw err;
        console.log("Database created");
      });
      
        var sql = "CREATE TABLE IF NOT EXISTS firstProject.user (id int PRIMARY KEY auto_increment,name VARCHAR(255), email VARCHAR(255),password VARCHAR(255),profileImage VARCHAR(255))";
        con.query(sql, function(err) {
            if (err) {
              console.log(err.message);
            }
            console.log("Table created");
          });
          var sql = "CREATE TABLE IF NOT EXISTS firstProject.comment (id int, CONSTRAINT id FOREIGN KEY (id)REFERENCES user(id),comment VARCHAR(255),time DATETIME,ratting DECIMAL)";
        con.query(sql, function(err) {
            if (err) {
              console.log(err.message);
            }
            console.log("Table comment created");
          });
          var sql = "CREATE TABLE IF NOT EXISTS firstProject.movie (id int,poster VARCHAR(1000),title VARCHAR(255),price int,des VARCHAR(255))";
          con.query(sql, function(err) {
              if (err) {
                console.log(err.message);
              }
              console.log("Table movie created");
            });
          
  });
module.exports = con;
