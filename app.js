const express = require("express");
const controller = require('./controllers/controller');
const cors = require("cors");
const app = express();
const cookieParser = require("cookie-parser");

app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded());
app.use(cors())
 
app.get('/products/:id', function (req, res, next) {
  res.json({msg: 'This is CORS-enabled for all origins!'})
})
//listen to port 
controller(app);
app.listen(4000);
console.log("app.js");

console.log("youre listing to port 4000");

